import axios from 'axios';
import { api_url, token } from '../config/api';

export function signup(data) {
    return axios({
        method: 'post',
        url: api_url + 'public/users/signup',
        data: data,
        headers: { Token: token },
        withCredentials: true
    })
}

export function signin(data) {
    return axios({
        method: 'post',
        url: api_url + 'public/users/signin',
        data: data,
        headers: { Token: token },
        withCredentials: true
    })
}