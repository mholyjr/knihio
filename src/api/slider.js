import axios from 'axios';
import { api_url, token } from '../config/api';

export function getSlides() {
    return axios({
        method: 'get',
        url: api_url + 'public/slider/get-slides',
        headers: { Token: token },
        withCredentials: true
    })
}