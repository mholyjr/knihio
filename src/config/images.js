export const logo = {
    color: require('../assets/images/logo/knihio_logo.svg')
}

export const background = {
    home_header: require('../assets/images/background/home_header_bg.svg')
}

export const illustrations = {
    pageNotFound: require('../assets/images/illustrations/page_not_found.svg'),
    registrace: require('../assets/images/illustrations/registrace.svg')
}