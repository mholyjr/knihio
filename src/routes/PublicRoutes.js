import React from 'react'
import { Route, Switch, withRouter } from 'react-router'
import { Container } from 'reactstrap'
import PageNotFound from '../components/views/404/PageNotFound'
import SignupView from '../components/views/Auth/SignupView'
import LoginView from '../components/views/Auth/LoginView'
import SuccessSignupView from '../components/views/Auth/SuccessSignupView'
import CategoryView from '../components/views/Category/CategoryView'
import HomeView from '../components/views/Home/HomeView'
import ContactView from '../components/views/Info/ContactView'
import PrettyLinksView from '../components/views/PrettyLinks/PrettyLinksView'
import ProductView from '../components/views/Product/ProductView'
import SearchResultsView from '../components/views/Search/SearchResultsView'
import { GlobalStyle } from '../styles/global'
import PrivateRoute from './PrivateRoute'
import UserProfileView from '../components/views/User/UserProfileView'

function PublicRoutes() {

    return (
        <>
        <GlobalStyle />
        <Switch>
            <Route path='/' component={withRouter(HomeView)} exact />
            <Route path='/knihy/kategorie/:alias/:genre?' component={withRouter(CategoryView)} exact />
            <Route path='/kniha/:alias/:id' component={withRouter(ProductView)} exact />
            <Route path='/koupit/:id' component={withRouter(PrettyLinksView)} exact />
            <Route path='/vyhledavani' component={withRouter(SearchResultsView)} exact />
            <Route path='/kontakty' component={withRouter(ContactView)} exact />
            <Route path='/registrace' component={withRouter(SignupView)} exact />
            <Route path='/registrace/uspech' component={withRouter(SuccessSignupView)} exact />
            <Route path='/prihlasit-se' component={withRouter(LoginView)} exact />
            <PrivateRoute path='/profil' component={withRouter(UserProfileView)} exact />
            <Route path='*' component={PageNotFound} />
        </Switch>
        </>
    )

}

export default PublicRoutes