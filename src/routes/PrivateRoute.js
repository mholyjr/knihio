import React from 'react'
import { Redirect, Route } from 'react-router'

function PrivateRoute ({ component: Component, ...rest }) {
    return (
        <Route {...rest} render={() => {
            return <Redirect to='/prihlasit-se' />
        }} />
      )
}

export default PrivateRoute