import React from 'react'
import { Row } from 'reactstrap'
import CategoryCardItem from './elems/CategoryCardItem'

function CategoriesCards({ cats }) {

    return (
        <Row>
            {cats.map((item) => {
                return (
                    <CategoryCardItem item={item} />
                )
            })}
        </Row>
    )
}

export default CategoriesCards