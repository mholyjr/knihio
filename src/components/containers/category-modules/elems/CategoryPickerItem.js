import React from 'react'
import { BubbleLink } from '../../../../styles/elems'

function CategoryPickerItem({ item }) {

    return (
        <BubbleLink bg='light' to={`/knihy/kategorie/${item.alias}`}>
            {item.title}
        </BubbleLink>
    )
}

export default CategoryPickerItem