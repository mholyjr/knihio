import React from 'react'
import { Link } from 'react-router-dom'
import { Col, Row } from 'reactstrap'
import { CategoryCard, CategoryTitle, Icon } from '../../../../styles/categoriesCards'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faBookReader } from '@fortawesome/free-solid-svg-icons'

function CategoryCardItem({ item }) {

    return (
        <Col sm={3}>
            <Link to={item.link}>
                <CategoryCard>
                    <Row className='align-items-center'>
                        <Col sm={3}>
                            <Icon icon={item.icon} />
                        </Col>
                        <Col sm={9}>
                            <CategoryTitle>{item.title}</CategoryTitle>
                        </Col>
                    </Row>
                </CategoryCard>
            </Link>
        </Col>
    )
}

export default CategoryCardItem