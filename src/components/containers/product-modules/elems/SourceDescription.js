import React, { useState } from 'react'
import { SourceDescriptionContainer } from '../../../../styles/pricesList'

function SourceDescription({ text }) {

    const [collapse, setCollapse] = useState(true)

    return (
        <SourceDescriptionContainer onClick={() => setCollapse(!collapse)} collapse={collapse}>
            {text}
        </SourceDescriptionContainer>
    )
}

export default SourceDescription