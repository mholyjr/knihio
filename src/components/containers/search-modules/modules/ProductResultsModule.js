import React, { useState, useEffect } from 'react'
import queryString from 'query-string'
import { searchProducts } from '../../../../api/search';
import CategoryProducts from '../../Category/CategoryProducts';
import { Col, Container, Row } from 'reactstrap';
import { Button } from 'semantic-ui-react';
import LoadingCategoryProducts from '../../Category/LoadingCategoryProducts';
import DefaultMessage from '../../Messages/DefaultMessage';

function ProductResultsModule({props}) {

    const parsed = queryString.parse(props.location.search);
    const [defaultLimit, setDefault] = useState(16);
    const [limit, setLimit] = useState(16);
    const [offset, setOffset] = useState(0);
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        getResults()
    }, [props, limit, offset])

    function showMore() {
        setLimit(limit + defaultLimit)
        setOffset(((limit / defaultLimit) - 1) * defaultLimit)
    }

    function getResults() {

        const query = {
            name: parsed.q ? parsed.q : null,
            id_category: parsed.cat ? parsed.cat : null,
            limit: limit,
            offset: offset
        }

        const req = queryString.stringify(query, {
            skipNull: true
        });

        searchProducts(req)
        .then((res) => {
            setData(res.data.data)
            setLoading(false)
        })
    }

    function renderItems() {
        if(!loading && data.length !== 0) {
            return (
                <CategoryProducts items={data} />
            )
        } else if(!loading && data.length === 0) {
            return (
                <DefaultMessage type='error' message='Omlouváme se, pro zadaný dotaz jsme nenašli žádné produkty.' />
            )
        } else if(loading) {
            return (
                <LoadingCategoryProducts />
            )
        }
    }

    return (
        <>
            
                {renderItems()}
                
                <Row>
                    <Col>
                        <Button onClick={() => showMore()}>Načíst další</Button>
                    </Col>
                </Row>
            
        </>
    )
}

export default ProductResultsModule