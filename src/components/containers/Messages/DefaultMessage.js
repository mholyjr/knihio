import React from 'react'
import { MessageContainer, MessageContent } from '../../../styles/messages'

function DefaultMessage({ message, type }) {

    return (
        <MessageContainer type={type}>
            <MessageContent>
                {message}
            </MessageContent>
        </MessageContainer>
    )
}

export default DefaultMessage