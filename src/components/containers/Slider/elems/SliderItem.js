import React from 'react'
import { Link } from 'react-router-dom'
import { Col, Row } from 'reactstrap'
import { Button } from 'semantic-ui-react'
import { slider_img_url } from '../../../../config/api'
import { SlideItem, SlideHeadline, SlideText, SlideButton, SlideProductImg } from '../../../../styles/slider'

function SliderItem({ item }) {
    return (
        <SlideItem img={slider_img_url + item.bg_image} bg_color={item.bg_color}>
            <Row className='align-items-center'>
                <Col sm={7}>
                    <SlideHeadline color={item.headline_color}>{item.headline}</SlideHeadline>
                    <SlideText color={item.text_color}>{item.text}</SlideText>
                    <SlideButton to={item.button_link} as={Link}>{item.button_text}</SlideButton>
                </Col>
                <Col sm={5} className='text-center'>
                    <SlideProductImg src={slider_img_url + item.product_image} />
                </Col>
            </Row>
        </SlideItem>
    )
}

export default SliderItem