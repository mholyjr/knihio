import React, { useState, useEffect } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Pagination, Autoplay } from 'swiper';
import 'swiper/swiper-bundle.min.css';
import { getSlides } from '../../../api/slider';
import SliderItem from './elems/SliderItem';

SwiperCore.use([Pagination, Autoplay]);

function Slider({ autoplay = false }) {

    const [slides, setSlides] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        getData()
    }, [])

    function getData() {
        getSlides()
        .then((res) => {
            if(res.data.type === 'success') {
                setSlides(res.data.data)
                setLoading(false)
            }
        })
        .catch((err) => {
            console.log(err)
        })
    }

    return (
        <>
        {!loading &&
        <Swiper
            spaceBetween={15}
            slidesPerView={1}
            pagination={{ clickable: true }}
            autoplay={autoplay ? { delay: 6000 } : false}
            breakpoints={{}}
        >
            {!loading && slides.map((item) => {
                return (
                    <SwiperSlide key={item.id_slide}>
                        <SliderItem item={item} />
                    </SwiperSlide>
                )
            })
            }
        </Swiper>
        }
        </>
    )
}

export default Slider