import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Col, Container, Row } from 'reactstrap'
import { getFooterNavigation } from '../../../api/navigation'
import { FooterBox, FooterContainer, FooterContent, FooterCopy, FooterNav, FooterNavItem, FooterNavLink, FooterSectionHeadline, FooterText } from '../../../styles/footer'

function Footer(props) {

    const [navigation, setNavigation] = useState([])
    const [loadingNav, setLoadingNav] = useState(true)

    useEffect(() => {

        getFooterNavigation()
        .then((res) => {
            setNavigation(res.data.data)
            setLoadingNav(false)
        })
    }, [])

    return (
        <FooterContainer>
            <FooterContent>
                <Container>
                    <Row>
                        <FooterBox sm={3}>
                            <FooterSectionHeadline>Knihio</FooterSectionHeadline>
                            <FooterText>
                                Knihio - vyhledávač knih s více než 70.000 tituly v nabídce. Najděte rychle a pohodlně svou příští oblíbenou knihu za nejlepší cenu.
                            </FooterText>
                        </FooterBox>
                        <FooterBox sm={9}>
                            <FooterSectionHeadline>Oblíbené kategorie</FooterSectionHeadline>
                            <FooterNav>
                                {navigation.map((item) => {
                                    return (
                                        <FooterNavItem key={item.alias}>
                                            <FooterNavLink to={`/knihy/kategorie/${item.alias}`}>{item.name}</FooterNavLink>
                                        </FooterNavItem>
                                    )
                                })}
                            </FooterNav>
                        </FooterBox>
                    </Row>
                </Container>
            </FooterContent>
            
            <FooterCopy>
                <Container>
                    <Row>
                        <Col sm={6}>
                            <span>&copy; {moment().format('YYYY')} Knihio.cz | Všechna práva vyhrazena</span>
                        </Col>
                        <Col sm={6} className='text-right'>
                            <Link to='/kontakty'>Kontaktujte nás</Link>
                        </Col>
                    </Row>
                </Container>
            </FooterCopy>
        </FooterContainer>
    )
}

export default Footer