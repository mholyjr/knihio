import React from 'react'
import { Col, Row } from 'reactstrap'
import { HomeHeaderContainer, HomeHeaderHeadline, HomeSubHeadline } from '../../../styles/elems'
import SearchForm from '../../forms/SearchForm'
import LastSearchesModule from '../search-modules/modules/LastSearchesModule'

function HomeHeader(props) {

    return (
        <HomeHeaderContainer>
            <Row>
                <Col sm={8} className='offset-sm-2' style={{marginBottom: 100}}>
                    <HomeSubHeadline>Knihio</HomeSubHeadline>
                    <HomeHeaderHeadline>Najděte svou knihu</HomeHeaderHeadline>
                    <SearchForm />
                    <LastSearchesModule bg="dark" />
                </Col>
            </Row>
        </HomeHeaderContainer>
    )
}

export default HomeHeader