import React, { useState } from 'react'
import { jsonToFormData } from '../../../tools'
import { Link, useHistory } from "react-router-dom"
import { Button, Form, Icon, Message } from 'semantic-ui-react'
import { signin } from '../../../api/auth';

function LoginForm() {

    let history = useHistory();

    const [loading, setLoading] = useState(false)
    const [data, setData] = useState({})
    const [errors, setError] = useState({})
    const [serverError, setServerError] = useState(false)
    const [serverSuccess, setServerSuccess] = useState(false)
    const [serverMessage, setServerMessage] = useState('')

    function onChange(e) {
        setData({...data, [e.target.name]: e.target.value})
        setError({...errors, [e.target.name]: false})
    }

    function submit() {
        setLoading(true)

            var userData = jsonToFormData(data)

            signin(userData)
            .then((res) => {
                if(res.data.type === 'error') {
                    setServerError(true)
                    setServerMessage(res.data.message)
                } else if(res.data.type === 'success') {
                    setServerSuccess(true)
                    setServerMessage(res.data.message)
                    history.push('/registrace/uspech')
                }
                setLoading(false)
            })
            .catch((err) => {
                alert('Vyskytla se chyba na naší straně. Zkuste prosím registraci později.')
            })
    }

    return (
        <Form onSubmit={submit} loading={loading} error={serverError} success={serverSuccess}>
            <Form.Input 
                type='email'
                name='email'
                label='Email'
                placeholder='Zadejte Váš email'
                required
                onChange={onChange}
            />
            <Form.Input 
                type='password'
                name='password'
                label='Heslo'
                placeholder='Zadejte Váše heslo'
                required
                onChange={onChange}
            />

            <Message
                error
                header={serverMessage}
            />
            <Message
                success
                icon
                header={serverMessage}
                icon={<Icon name='circle notched' loading />}
            />

            <Button primary fluid>Přihlásit se</Button>

            <div className='mt-3'>
                <p className='text-center'>Nemáte účet? <Link to='/registrace'>Zaregistrujte se</Link></p>
            </div>

        </Form>
    )
}

export default LoginForm