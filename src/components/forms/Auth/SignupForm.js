import React, { useState, useEffect } from 'react'
import { Button, Checkbox, Form, Icon, Message } from 'semantic-ui-react'
import { signup } from '../../../api/auth'
import { jsonToFormData } from '../../../tools'
import { Link, useHistory } from "react-router-dom"

function SignupForm(props) {

    let history = useHistory();

    const [loading, setLoading] = useState(false)
    const [data, setData] = useState({})
    const [errors, setError] = useState({})
    const [serverError, setServerError] = useState(false)
    const [serverSuccess, setServerSuccess] = useState(false)
    const [serverMessage, setServerMessage] = useState('')

    function onChange(e) {
        setData({...data, [e.target.name]: e.target.value})
        setError({...errors, [e.target.name]: false})
    }

    function onChangeCheckbox(e, input) {
        let checked = input.checked ? 1 : 0
        setData({...data, [input.name]: checked})
    }

    function validate() {

        var isValid = true

        if(data.password !== data.password_check) {
            isValid = false
            setError({...errors, password_check: 'Zadaná hesla se neshodují'})
        }

        if(data.password.length < 6) {
            isValid = false
            setError({...errors, password: 'Heslo musí mít minimálně 6 znaků'})
        }

        if(!validateEmail(data.email)) {
            isValid = false
            setError({...errors, email: 'Email není ve správném tvaru'})
        }

        if(!validateName(data.name)) {
            isValid = false
            setError({...errors, name: 'Ve jméně mohou být pouze písmena a mezery'})
        }


        return isValid

    }

    function validateEmail(email) {
        const re = /\S+@\S+\.\S+/;
        return re.test(String(email).toLowerCase());
    }

    function validateName(name) {
        const re = /^[ a-zA-ZÀ-ÿ\u00f1\u00d1]*$/g;
        return re.test(String(name).toLowerCase());
    }

    function submit() {
        setLoading(true)

        if(validate()) {
            var userData = jsonToFormData(data)

            signup(userData)
            .then((res) => {
                if(res.data.type === 'error') {
                    setServerError(true)
                    setServerMessage(res.data.message)
                } else if(res.data.type === 'success') {
                    setServerSuccess(true)
                    setServerMessage(res.data.message)
                    history.push('/registrace/uspech')
                }
                setLoading(false)
            })
            .catch((err) => {
                alert('Vyskytla se chyba na naší straně. Zkuste prosím registraci později.')
            })

        } else {
            setLoading(false)
        }
    }

    return (
        <>
            <Form loading={loading} onSubmit={submit} error={serverError} success={serverSuccess}>
                <Form.Input 
                    type='text'
                    name='name'
                    id="signup_name"
                    placeholder='Zadejte Vaše jméno'
                    label='Vaše jméno'
                    onChange={onChange}
                    error={errors.name}
                />
                
                <Form.Input 
                    type='email'
                    name='email'
                    id='signup_email'
                    placeholder='Zadejte Váš email'
                    label='Email'
                    required
                    onChange={onChange}
                    error={errors.email}
                />
                <Form.Input 
                    name='password'
                    type='password'
                    id='signup_password'
                    placeholder='Zadejte heslo'
                    label='Heslo'
                    required
                    onChange={onChange}
                    error={errors.password}
                />
                <Form.Input 
                    name='password_check'
                    type='password'
                    id='signup_password_check'
                    placeholder='Zadejte heslo znovu'
                    label='Heslo znovu'
                    required
                    onChange={onChange}
                    error={errors.password_check}
                />
                <Form.Field>
                    <Checkbox 
                        label={<label>Souhlasím s Všeobecnými obchodními podmínkami</label>}
                        name='terms'
                        onChange={onChangeCheckbox}
                    />
                </Form.Field>
                <Form.Field>
                    <Checkbox
                        label={<label>Souhlasím se Zásadami zpracování osobních údajů</label>}
                        name='gdpr'
                        onChange={onChangeCheckbox}
                    />
                </Form.Field>
                <Message
                    error
                    header={serverMessage}
                />
                <Message
                    success
                    icon
                    header={serverMessage}
                    icon={<Icon name='circle notched' loading />}
                />
                <Button primary fluid disabled={data.gdpr !== 1 || data.terms !== 1}>Zaregistrovat se</Button>
                <div className='mt-3'>
                    <p className='text-center'>Máte již účet? <Link to='/prihlasit-se'>Přihlašte se</Link></p>
                </div>
            </Form>
        </>
    )

}

export default SignupForm