import React from 'react'
import Col from 'reactstrap/lib/Col'
import Container from 'reactstrap/lib/Container'
import Row from 'reactstrap/lib/Row'
import { FormContainer, FormHead, FormTitle } from '../../../styles/forms'
import SignupForm from '../../forms/Auth/SignupForm'

function SignupView(props) {

    return (
        <Container>
            <Row>
                <Col sm={4} className='offset-sm-4'>
                    <FormContainer>
                        <FormHead>
                            <FormTitle>Registrace na Knihio.cz</FormTitle>
                        </FormHead>
                        <SignupForm props={props} />
                    </FormContainer>
                </Col>
            </Row>
        </Container>
    )

}

export default SignupView