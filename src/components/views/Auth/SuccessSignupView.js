import React from 'react'
import { Col, Row } from 'reactstrap'
import { illustrations } from '../../../config/images'
import { BubbleLink, MainIllustration } from '../../../styles/elems'
import { LoaderContainer } from '../../../styles/prettyLinks'
import { PageTitle } from '../../../styles/sections'

function SuccessSignupView(props) {

    return (
        <LoaderContainer style={{paddingTop: 70, paddingBottom: 70}}>
            <Row>
                <Col>
                    <MainIllustration src={illustrations.registrace} style={{maxWidth: 300}} />
                    <PageTitle className='text-center'>Jste zaregistrováni!</PageTitle>
                    <p className='text-center my-5'>Na zadaný email jsme Vám poslali aktivační kód. Po aktivaci účtu se můžete přihlásit. Zkontrolujte i složku Spam.</p>
                </Col>
            </Row>
        </LoaderContainer>
    )

}

export default SuccessSignupView