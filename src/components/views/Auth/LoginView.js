import React from 'react'
import Col from 'reactstrap/lib/Col'
import Container from 'reactstrap/lib/Container'
import Row from 'reactstrap/lib/Row'
import { FormContainer, FormHead, FormTitle } from '../../../styles/forms'
import LoginForm from '../../forms/Auth/LoginForm'

function LoginView(props) {

    return (
        <Container>
            <Row>
                <Col sm={4} className='offset-sm-4'>
                    <FormContainer>
                        <FormHead>
                            <FormTitle>Přihlásit se</FormTitle>
                        </FormHead>
                        <LoginForm props={props} />
                    </FormContainer>
                </Col>
            </Row>
        </Container>
    )

}

export default LoginView