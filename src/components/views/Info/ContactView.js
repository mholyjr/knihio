import React from 'react'
import Col from 'reactstrap/lib/Col'
import Container from 'reactstrap/lib/Container'
import Row from 'reactstrap/lib/Row'
import { ContactLabel, Email } from '../../../styles/contacts'
import { PageSection, PageTitle, SectionHeading } from '../../../styles/sections'
import { theme } from '../../../styles/vars'

function ContactView() {

    return (
            <>
                <PageSection bg={theme.bg_secondary}>
                    <Container>
                        <PageTitle>Kontakty</PageTitle>
                    </Container>
                </PageSection>

                <Container>
                <Row>
                    <Col sm={6}>
                        <PageSection>
                            <SectionHeading>Napište nám</SectionHeading>
                            <Email href='mailto:info@knihio.cz'>info@knihio.cz</Email>
                            <p>Jsme tu pro vás. Neváhejte se na nás obrátit s každým dotazem, připomínkou nebo nápadem na zlepšení našich služeb.</p>
                        </PageSection>
                    </Col>
                    <Col sm={3}>
                        <PageSection>
                            <SectionHeading>Korespondenční adresa</SectionHeading>
                            <p>
                                Ještědská 121<br />
                                148 00, Praha 4<br />
                            </p>
                        </PageSection>
                    </Col>
                    <Col sm={3}>
                        <PageSection>
                            <SectionHeading>Fakturační adresa</SectionHeading>
                            <p>
                                Martin Holý<br />
                                Šrámkova 164<br />
                                460 10, Liberec<br />
                            </p>
                            <p>
                                IČ: 08191131<br />
                                Nejsme plátci DPH
                            </p>
                        </PageSection>
                    </Col>
                </Row>
                </Container>
            </>        
    )
}

export default ContactView