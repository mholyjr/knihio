import React, {useEffect} from 'react'
import { getLink } from '../../../api/products';
import { LoaderContainer } from '../../../styles/prettyLinks';
import { Loader } from 'semantic-ui-react';
import ReactGA from 'react-ga'
import { Helmet } from 'react-helmet'
import ReactPixel from 'react-facebook-pixel'

function PrettyLinksView(props) {

    const { id } = props.match.params

    useEffect(() => {

        ReactPixel.pageView();
        ReactGA.pageview(props.location.pathname + props.location.search);

        getLink(id)
        .then((res) => {
            if(res.data.type === 'success') {
                window.location.href = res.data.link

                ReactPixel.trackCustom('LinkClick', { id: id });

                ReactGA.event({
                    category: 'Clicks',
                    action: 'Click',
                    label: 'Links'
                });
            }
        })
    }, [])
    

    return (
        <LoaderContainer>
            <Helmet
                script={[{ 
                    type: 'text/javascript', 
                    innerHTML: 'gtag(\'event\', \'conversion\', {\'send_to\': \'AW-879436683/Qh_tCPitke4BEIvHrKMD\'});' 
                }]} 
            />
            <Loader active>Přesměrovávám do obchodu</Loader>
        </LoaderContainer>
    )
}

export default PrettyLinksView