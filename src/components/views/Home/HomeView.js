import React, { useEffect } from 'react'
import { isMobileOnly } from 'react-device-detect'
import { Container } from 'reactstrap'
import { PageSection, SectionHeading } from '../../../styles/sections'
import { theme } from '../../../styles/vars'
import HomeHeader from '../../containers/Home/HomeHeader'
import MobileHomeHeader from '../../containers/Home/MobileHomeHeader'
import MostViewedContainer from '../../containers/product-modules/MostViewedContainer'
import ReactGA from 'react-ga'
import ReactPixel from 'react-facebook-pixel'
import CategoriesCards from '../../containers/category-modules/CategoriesCards'
import { faChild, faBookReader, faGlobeEurope, faUtensils, faUniversity, faLightbulb, faCalendarAlt, faHeart, faHeartbeat } from '@fortawesome/free-solid-svg-icons'
import Slider from '../../containers/Slider/Slider'
import { settings } from '../../../config/settings'

function HomeView(props) {

    const categories = [
        {
            title: 'Beletrie pro dospělé',
            link: '/knihy/kategorie/beletrie-pro-dospele',
            icon: faBookReader
        },
        {
            title: 'Beletrie pro děti',
            link: '/knihy/kategorie/beletrie-pro-deti',
            icon: faChild
        },
        {
            title: 'Knihy o cestování',
            link: '/knihy/kategorie/cestovani',
            icon: faGlobeEurope
        },
        {
            title: 'Kuchařky',
            link: '/knihy/kategorie/kucharky',
            icon: faUtensils
        },
        {
            title: 'Naučná literatura',
            link: '/knihy/kategorie/naucna-literatura',
            icon: faLightbulb
        },
        {
            title: 'Odborná literatura',
            link: '/knihy/kategorie/odborna-literatura',
            icon: faUniversity
        },
        {
            title: 'Knihy o historii',
            link: '/knihy/kategorie/historie-a-military',
            icon: faCalendarAlt
        },
        {
            title: 'Knihy o zdraví',
            link: '/knihy/kategorie/zdravi-a-zivotni-styl',
            icon: faHeartbeat
        }
    ]

    useEffect(() => {
        ReactGA.pageview(props.location.pathname + props.location.search);
        ReactPixel.pageView();
        window.scrollTo(0, 0);
        document.title = 'Přes 70.000 knih na jednom místě | Knihio'
    }, [props])

    return (
        <>
            {isMobileOnly ? 
            <MobileHomeHeader />
            :
            <HomeHeader />
            }

            <PageSection>
                <Container>
                    <SectionHeading>Oblíbené kategorie</SectionHeading>
                    <CategoriesCards cats={categories} />
                </Container>
            </PageSection>

            {settings.show_slider &&
            <PageSection>
                <Container>
                    <Slider autoplay={true} />
                </Container>
            </PageSection>
            }
            
            <PageSection>
                <Container>
                    <SectionHeading>Nejoblíbenější knihy</SectionHeading>
                    <MostViewedContainer type='most-viewed' autoplay={true} />
                </Container>
            </PageSection>
            <PageSection bg={theme.bg_secondary}>
                <Container>
                    <SectionHeading>Nejnovější knihy</SectionHeading>
                    <MostViewedContainer type='new' autoplay={true} />
                </Container>
            </PageSection>
        </>
    )

}

export default HomeView