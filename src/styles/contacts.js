import styled from 'styled-components'
import { theme } from './vars'

export const Email = styled.a`
    font-size: 2.5rem;
    font-weight: 700;
    color: ${theme.title};
`

export const ContactLabel = styled.p`
    margin-bottom: 0;
`