import styled from 'styled-components'
import { defaultBorder, rad1, shadow1 } from './vars'

export const FormContainer = styled.div`
    width: 100%;
    border-radius: ${rad1};
    box-shadow: ${shadow1};
    padding: 1.5rem;
    margin-top: 4rem;
    margin-bottom: 4rem;
`

export const FormHead = styled.div`
    border-bottom: ${defaultBorder};
    padding-bottom: 3rem;
    margin-bottom: 3rem;
    padding-top: 1.5rem;
`

export const FormTitle = styled.h2`
    text-align: center;
    font-size: 22px;
    font-weight: 700;
`