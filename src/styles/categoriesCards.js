import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled from 'styled-components'
import { defaultBorder, rad2, shadow1, theme } from './vars'

export const Icon = styled(FontAwesomeIcon)`
    font-size: 40px;
    color: ${theme.accent};
    transition: all .3s ease-out;
`

export const CategoryTitle = styled.h2`
    font-size: 18px;
    font-weight: 700;
    color: ${theme.title};
    transition: all .3s ease-out;
`

export const CategoryCard = styled.div`
    width: 100%;
    padding: 1.5rem;
    border-radius: ${rad2};
    border: ${defaultBorder};
    margin-bottom: 30px;
    transition: all .3s ease-out;

    :hover {
        border-color: transparent;
        box-shadow: ${shadow1};
        background: ${theme.accent};

        ${Icon}, ${CategoryTitle} {
            color: #fff;
        }
    }
`