import { Button } from 'semantic-ui-react'
import styled from 'styled-components'
import { rad1, rad2, shadow1, theme } from './vars'

export const SlideItem = styled.div`
    width: 100%;
    background-image: url("${props => props.img}");
    background-color: ${props => props.bg_color};
    background-size: cover;
    background-position: center center;
    border-radius: ${rad1};
    padding: 6rem;
    min-height: 555px;
    position: relative;
    display: flex;
    align-items: center;
`

export const SlideHeadline = styled.h2`
    font-size: 52px;
    font-weight: 700;
    margin-bottom: 30px;

    color: ${props => props.color === 'light' ? '#fff' : theme.title};
`

export const SlideText = styled.p`
    color: ${props => props.color === 'light' ? '#fff' : theme.title};
    font-size: 26px;
`

export const SlideButton = styled(Button)`
    display: inline-block;
    border-radius: ${rad2};
    padding: 20px 60px !important;
    background-color: #fff !important;
    color: ${theme.primary} !important;
    margin-top: 20px !important;
    font-weight: 700;

    :hover {
        background-color: ${theme.primary} !important;
        color: #fff !important;
    }
`

export const SlideProductImg = styled.img`
    max-height: 420px;
    max-width: 100%;
    border-radius: ${rad2};
`