import styled from 'styled-components'
import { rad2 } from './vars'

export const MessageContainer = styled.div`
    width: 100%;
    border-radius: ${rad2};
    padding: 1.5rem;
    margin-bottom: 3rem;

    ${props => props.type === 'error' && 'background: #ffe8e6; border: 1px solid #db2828; color: #db2828;'}
`

export const MessageContent = styled.div`
    width: 100%;
    font-weight: 700;
`
