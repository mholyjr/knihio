import React from 'react';
import NavigationContainer from './components/containers/Navigation/NavigationContainer';
import PublicRoutes from './routes/PublicRoutes';
import { isMobileOnly } from 'react-device-detect';
import MobileNavigationContainer from './components/containers/Navigation/MobileNavigationContainer';
import ReactGA from 'react-ga'
import Footer from './components/containers/Footer/Footer';
import { Helmet } from 'react-helmet'
import ReactPixel from 'react-facebook-pixel'

function App(props) {

  if(!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {

  } else {
    ReactGA.initialize('UA-185326477-1', { debug: false, });
    const advancedMatching = {};
    const options = {
      autoConfig: true,
      debug: false
    };
    ReactPixel.init('836139523842417', advancedMatching, options);
  }
  

  return (
    <>
    <Helmet>
      <meta name="description" content="Knihio - vyhledávač knih s více než 70.000 tituly v nabídce. Najděte rychle a pohodlně svou příští oblíbenou knihu za nejlepší cenu." />
    </Helmet>
    <Helmet script={[
      {
        type: 'text/javascript', 
        innerHTML: 'window.addEventListener("load", function(){ window.cookieconsent.initialise({ "palette": { "popup": { "background": "#edeff5", "text": "#333" }, "button": { "background": "rgb(235, 152, 58)" } }, "content": { "message": "Tento web využívá soubory cookies. Prohlížením webu vyjadřujete souhlas s jejich používáním.", "dismiss": "Rozumím", "link": "Více informací", "href": "https://policies.google.com/technologies/cookies?hl=cs" } })})' 
      }
    ]} />
    {isMobileOnly ?
    <MobileNavigationContainer props={props} />
    :
    <NavigationContainer props={props} />
    }
    <PublicRoutes />
    <Footer />
    </>
  );
}

export default App;
